FROM node:13.12.0-alpine

WORKDIR /app

# # add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

COPY package.json ./
COPY .env .
COPY . .

RUN npm install

CMD ["npm", "start"]
