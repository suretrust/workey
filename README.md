## Welcome to Workey!
Workey is a web app used to control and record the working hours for a company's employees. The app will be used by each employee to manage and keep account of worked hours, Arriving and Exiting hours, and Lunch breaks.

### Details
#### Lunch hours
This is calculated based on the total number of hours spent at launch.

#### Expected hours
This is 8 hours per day.

#### Worked hours
By default, this is 0, it increases based on the following calculation.

- Working hours = Time of arrival — Closing time
- Worked hours = Working hours — Lunch hours
### Basic Constraints
- You can't add work hours for a future date.
- Worked hours are sorted by latest date on top.
- A user can't add worked hours for same date more than once.
- The least time for a launch break is 15 mins.
### Features I'd love to add
- Add prop-types validation.
- Add more test coverage.
- Confirm password for sign up screen.
- Restrict adding arrival, exit, and launch times to a button click on start/stop so it will be using live update method instead of directly inputting a value.
### Default login details (you can choose to sign up)
- Email: klmk@fakl.com
- Password: 123456

### How to run using npm
- Run `npm install`
- Run `npm start`

### How to run using docker
- Run `npm run docker`
- Run `npm run docker-start`

### For tests, run `npm run test`