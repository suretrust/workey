import { render } from '@testing-library/react'
import LoginSignUpForm from '.'
import TestRoute from '../../utilities/testUtil'

const props = {
  title: 'test title',
  subtitle: 'test subtitle',
  handleSubmit: jest.fn(),
  userDetails: { email: 'test@test.com', password: '123456' },
  setUserDetails: jest.fn(),
  error: null,
  setError: jest.fn(),
  footerText: 'test footer text',
  footerLink: { text: 'test text', href: '/test-link' }
}

describe('LoginSignUpForm component', () => {
  it('renders LoginSignUpForm', () => {
    const component = render(
      <TestRoute>
        <LoginSignUpForm {...props} />
      </TestRoute>
    )

    const LoginSignUpFormComponent = component.getByTestId('login-sign-up-form')
    expect(LoginSignUpFormComponent).toBeInTheDocument()
  })

  it('renders the title twice', () => {
    const component = render(
      <TestRoute>
        <LoginSignUpForm {...props} />
      </TestRoute>
    )

    const Titles = component.getAllByText(props.title)
    expect(Titles.length).toBe(2)
  })

  it('renders the subtitle', () => {
    const component = render(
      <TestRoute>
        <LoginSignUpForm {...props} />
      </TestRoute>
    )

    const subtitleText = component.getByText(props.subtitle)
    expect(subtitleText).toBeInTheDocument()
  }) 
})
