import styled from 'styled-components'

export const LoginSignUpFormWrapper = styled.div`
  background-color: rgba(221, 220, 220, 0.5);
  height: calc(100vh - 30px);
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding: 15px;

  h1,
  h2 {
    margin-top: 0;
    text-transform: uppercase;
    letter-spacing: 1px;
    font-weight: 800;
  }
`

export const LoginSignUpFormContainer = styled.form`
  max-width: 600px;

  input {
    width: 100%;
    box-sizing: border-box;
    border-radius: 5px;
    border: none;
    padding: 10px;
    margin-bottom: 15px;
    outline: none;

    ::placeholder {
      font-size: 16px;
      line-height: 19px;
    }
  }
`

export const Hr = styled.hr`
    margin-top: 50px;
`
export const ErrorMsg = styled.div`
    color: red;
    font-size: 14px;
    margin-bottom: 20px;
    text-align: center;
`

export const Logo = styled.img`
    width: 70px;
    height: 70px;
    margin-bottom: 30px;
`