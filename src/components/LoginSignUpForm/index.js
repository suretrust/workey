import React from 'react'
import { Link } from 'react-router-dom'
import Button from '../Button'
import {
  LoginSignUpFormWrapper,
  LoginSignUpFormContainer,
  Hr,
  ErrorMsg,
  Logo
} from './styled'

const LoginSignUpForm = ({
  title,
  subtitle,
  handleSubmit,
  userDetails,
  setUserDetails,
  error,
  setError,
  footerText,
  footerLink
}) => {
  const { text, href } = footerLink
  const handleChange = event => {
    const { name, value } = event.target
    setError('')
    setUserDetails({ ...userDetails, [name]: value })
  }

  return (
    <LoginSignUpFormWrapper data-testid='login-sign-up-form'>
      <Logo src='../../logo.svg' alt='logo' />
      <h2>{title}</h2>
      <p>{subtitle}</p>
      <LoginSignUpFormContainer onSubmit={handleSubmit}>
        {error && <ErrorMsg>{error}</ErrorMsg>}
        <input
          placeholder='Email'
          name='email'
          type='email'
          onChange={handleChange}
          value={userDetails.email}
          required
        />
        <input
          placeholder='Password'
          name='password'
          onChange={handleChange}
          value={userDetails.password}
          type='password'
          min='6'
          required
        />
        <Button type='submit' primary width={100} marginTop>
          {title}
        </Button>
        <Hr />
        <p>
          {footerText} <Link to={href}>{text}</Link>
        </p>
      </LoginSignUpFormContainer>
    </LoginSignUpFormWrapper>
  )
}

export default LoginSignUpForm
