import React from 'react'
import { getTimeFromMilliseconds } from '../../utilities/dateTime'
import {
  DateContainer,
  HoursContainer,
  HoursTitle,
  HoursValue,
  SingleDailyWorkHoursWrapper,
  TimeSpentInOfficeWrapper
} from './styled'

const SingleDailyWorkHours = ({
  workedHours,
  arrivalTime,
  exitTime,
  launchHours,
  dateOfWorkedHour
}) => {
  return (
    <SingleDailyWorkHoursWrapper data-testid='single-daily-work-hours'>
      <DateContainer>{dateOfWorkedHour}</DateContainer>
      <HoursContainer>
        <div>
          <HoursTitle launch>
            <span>Launch</span>
            <span>{launchHours}</span>
          </HoursTitle>
          <HoursTitle worked>
            <span>Worked</span>
            <span>{workedHours}</span>
          </HoursTitle>
          <HoursTitle>
            <span>Expected</span>
            <span>8.00</span>
          </HoursTitle>
        </div>
        <HoursValue>Hrs</HoursValue>
      </HoursContainer>

      <TimeSpentInOfficeWrapper>
        <small>
          <b>Resumed:</b> {getTimeFromMilliseconds(arrivalTime)}
        </small>
        <small>
          <b>Closed:</b> {getTimeFromMilliseconds(exitTime)}
        </small>
      </TimeSpentInOfficeWrapper>
    </SingleDailyWorkHoursWrapper>
  )
}

export default SingleDailyWorkHours
