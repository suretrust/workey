import styled from 'styled-components'

export const SingleDailyWorkHoursWrapper = styled.div`
  height: 150px;
  margin-bottom: 20px;
  border-radius: 5px;
  background-color: #f0f0f0;
  padding: 20px;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
`

export const HoursContainer = styled.div`
  display: grid;
  grid-template-columns: 140px auto;
`

export const HoursTitle = styled.div`
  font-weight: bold;
  text-transform: uppercase;
  display: flex;
  justify-content: space-between;
  font-family: 'Ranchers', cursive;
  color: ${props =>
    props.launch ? 'red' : props.worked ? 'green' : '#635d5d'};
  letter-spacing: 3px;
`

export const HoursValue = styled.div`
  height: 100%;
  font-size: 63px;
  line-height: 58px;
  text-transform: uppercase;
  color: #635d5d;
  font-family: 'Ranchers', cursive;
`

export const DateContainer = styled.small`
  position: absolute;
  left: 0;
  top: 0;
  padding: 10px;
  font-weight: bold;
  background: #d6d6d6;
  color: #635d5d;
  border-top-left-radius: 5px;
  text-transform: uppercase;
`

export const TimeSpentInOfficeWrapper = styled.div`
  position: absolute;
  right: 0;
  bottom: 0;
  padding: 10px;
  background: #d6d6d6;
  color: #635d5d;
  text-transform: uppercase;
  border-bottom-right-radius: 5px;

  small {
    padding: 0 5px;
  }
`
