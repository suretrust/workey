import { render } from '@testing-library/react'
import SingleDailyWorkHours from '.'

describe('SingleDailyWorkHours component', () => {
  it('renders SingleDailyWorkHours', () => {
    const component = render(<SingleDailyWorkHours />)

    const SingleDailyWorkHoursComponent = component.getByTestId(
      'single-daily-work-hours'
    )
    expect(SingleDailyWorkHoursComponent).toBeInTheDocument()
  })
})
