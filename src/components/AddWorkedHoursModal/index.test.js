import { render } from '@testing-library/react'
import AddWorkedHoursModal from '.'

describe('AddWorkedHoursModal component', () => {
  it('renders AddWorkedHoursModal', () => {
    const component = render(<AddWorkedHoursModal />)

    const addHoursComponent = component.getByTestId('add-hours')
    expect(addHoursComponent).toBeInTheDocument()
  })

  it('renders arrival time', () => {
    const component = render(<AddWorkedHoursModal />)

    const arrivalTime = component.getByText('Arrival time')
    const placeholder = component.getByPlaceholderText('Set your arrival time')

    expect(placeholder).toBeInTheDocument()
    expect(arrivalTime).toBeInTheDocument()
  })

  it('renders exit time', () => {
    const component = render(<AddWorkedHoursModal />)

    const exitTime = component.getByText('Exit time')
    const placeholder = component.getByPlaceholderText('Set your exit time')

    expect(placeholder).toBeInTheDocument()
    expect(exitTime).toBeInTheDocument()
  })

  it('renders launch start time', () => {
    const component = render(<AddWorkedHoursModal />)

    const launchStartTime = component.getByText('Launch start time')
    const placeholder = component.getByPlaceholderText(
      'Set your launch start time'
    )

    expect(placeholder).toBeInTheDocument()
    expect(launchStartTime).toBeInTheDocument()
  })

  it('renders launch stop time', () => {
    const component = render(<AddWorkedHoursModal />)

    const launchStopTime = component.getByText('Launch stop time')
    const placeholder = component.getByPlaceholderText(
      'Set your launch stop time'
    )

    expect(placeholder).toBeInTheDocument()
    expect(launchStopTime).toBeInTheDocument()
  })

  it('renders add worked hours button', () => {
    const component = render(<AddWorkedHoursModal />)

    const addWorkedHoursBtn = component.getByText('Add worked hours')

    expect(addWorkedHoursBtn).toBeInTheDocument()
  })

  it('renders close button', () => {
    const component = render(<AddWorkedHoursModal />)

    const addWorkedHoursBtn = component.getByText('Close')

    expect(addWorkedHoursBtn).toBeInTheDocument()
  })
})
