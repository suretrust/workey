import styled from 'styled-components'

export const FormWrapper = styled.div`
  position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0, 0, 0, 0.5);
  z-index: 1;
  display: flex;
  justify-content: center;
  align-items: center;
  box-sizing: border-box;
`

export const Form = styled.form`
  display: flex;
  justify-content: center;
  flex-direction: column;
  margin: 0 15px;
  padding: 20px;
  max-width: 600px;
  width: 400px;
  min-height: 100px;
  background: #635d5d;
  border-radius: 5px;
  color: white;

  .react-datepicker-wrapper {
    width: 100%;

    .react-datepicker__input-container {
      width: 100%;
    }

    input {
      padding: 10px;
      box-sizing: border-box;
      width: 100%;
      border-radius: 5px;
      border: none;
      outline: none;
      cursor: pointer;
      margin-bottom: 20px;

      :disabled {
        cursor: not-allowed;

        ::placeholder {
          color: lightgray;
        }
      }
    }
  }
`

export const Label = styled.label`
  text-transform: uppercase;
  margin-bottom: 5px;
  font-weight: bold;
  font-size: 14px;
  line-height: 16px;
`
