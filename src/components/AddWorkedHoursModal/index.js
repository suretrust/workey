import React, { useState } from 'react'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css';
import api from '../../helpers/api'
import { getEndOfTheDay, getWorkedHours } from '../../utilities/dateTime'
import Button from '../Button'
import { Form, FormWrapper, Label } from './styled'

const AddWorkedHoursModal = ({
  daysWithExistingHours,
  setWorkedHours,
  setShowAddWorkHoursModal
}) => {
  const [launchStartTime, setLaunchStartTime] = useState(null)
  const [launchStopTime, setLaunchStopTime] = useState(null)
  const [arrivalTime, setArrivalTime] = useState(null)
  const [exitTime, setExitTime] = useState(null)

  const isDisabled = !(
    launchStartTime &&
    launchStopTime &&
    arrivalTime &&
    exitTime
  )

  const handleSubmit = async e => {
    e.preventDefault()
    const timeStamps = {
      arrivalTime,
      exitTime,
      launchStartTime,
      launchStopTime
    }

    const workedHours = getWorkedHours({ ...timeStamps })
    if (!workedHours) return

    const workingHoursData = { ...timeStamps, workedHours }
    await api.addWorkedHours(workingHoursData)

    setWorkedHours(workedHours)

    setShowAddWorkHoursModal(false)
  }

  const handleCancel = e => {
    e.preventDefault()

    setShowAddWorkHoursModal(false)
  }

  return (
    <FormWrapper>
      <Form data-testid='add-hours'>
        <Label>Arrival time</Label>
        <DatePicker
          selected={arrivalTime}
          placeholderText='Set your arrival time'
          onChange={date => setArrivalTime(date.getTime())}
          showTimeSelect
          maxDate={new Date()}
          excludeDates={daysWithExistingHours}
          timeIntervals={15}
          dateFormat='Pp'
        />
        <Label>Exit time</Label>
        <DatePicker
          selected={exitTime}
          placeholderText='Set your exit time'
          onChange={date => setExitTime(date.getTime())}
          showTimeSelect
          minDate={arrivalTime}
          maxDate={arrivalTime}
          minTime={arrivalTime}
          maxTime={getEndOfTheDay(arrivalTime)}
          excludeDates={daysWithExistingHours}
          timeIntervals={15}
          dateFormat='Pp'
          disabled={!arrivalTime}
        />
        <Label>Launch start time</Label>
        <DatePicker
          selected={launchStartTime}
          placeholderText='Set your launch start time'
          onChange={date => setLaunchStartTime(date.getTime())}
          showTimeSelect
          minDate={arrivalTime}
          maxDate={arrivalTime}
          minTime={arrivalTime}
          maxTime={getEndOfTheDay(arrivalTime)}
          excludeDates={daysWithExistingHours}
          timeIntervals={15}
          dateFormat='Pp'
          disabled={!arrivalTime}
        />
        <Label>Launch stop time</Label>
        <DatePicker
          marginBottom
          selected={launchStopTime}
          placeholderText='Set your launch stop time'
          onChange={date => setLaunchStopTime(date.getTime())}
          showTimeSelect
          minDate={arrivalTime}
          maxDate={arrivalTime}
          minTime={launchStartTime + 100000}
          maxTime={getEndOfTheDay(arrivalTime)}
          excludeDates={daysWithExistingHours}
          timeIntervals={15}
          dateFormat='Pp'
          disabled={!launchStartTime}
        />
        <div>
          <Button onClick={handleCancel} marginRight>
            Close
          </Button>
          <Button onClick={handleSubmit} disabled={isDisabled} primary>
            Add worked hours
          </Button>
        </div>
      </Form>
    </FormWrapper>
  )
}

export default AddWorkedHoursModal
