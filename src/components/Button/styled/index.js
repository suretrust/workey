import styled from 'styled-components'

export const ButtonWrapper = styled.button`
  padding: ${props => props.width === 100 ? '15px 20px' : '10px 15px'};
  font-size: ${props => props.width === 100 && '16px'};
  text-transform: uppercase;
  letter-spacing: 0.5px;
  background-color: ${props => (props.primary ? '#d1e2ce' : '#d1aeae')};
  margin-right: ${props => (props.marginRight ? 10 : 0)}px;
  margin-top: ${props => (props.marginTop ? 10 : 0)}px;
  width: ${props => props.width}%;
  border-radius: 5px;
  border: none;
  cursor: pointer;
  outline: none;

  :disabled {
      cursor: not-allowed;
  }
`
