import { render } from '@testing-library/react'
import Button from '.'

describe('Button component', () => {
  it('renders Button', () => {
    const component = render(<Button />)

    const ButtonComponent = component.getByTestId('button')
    expect(ButtonComponent).toBeInTheDocument()
  })
})
