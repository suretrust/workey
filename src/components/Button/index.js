import React from 'react'
import { ButtonWrapper } from './styled'

const Button = ({
  children,
  onClick,
  type,
  primary,
  marginRight,
  marginTop,
  disabled,
  width
}) => {
  return (
    <ButtonWrapper
      data-testid='button'
      type={type}
      disabled={disabled}
      primary={primary}
      marginRight={marginRight}
      onClick={onClick}
      width={width}
      marginTop={marginTop}
    >
      {children}
    </ButtonWrapper>
  )
}

export default Button
