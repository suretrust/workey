import { render } from '@testing-library/react'
import Layout from '.'

describe('Layout component', () => {
  it('renders Layout', () => {
    const component = render(<Layout />)

    const layoutComponent = component.getByTestId('layout')
    expect(layoutComponent).toBeInTheDocument()
  })

  it('renders sign out button', () => {
    const component = render(<Layout />)
    const signOutBtn = component.getByText('Sign Out')

    expect(signOutBtn).toBeInTheDocument()
  })

  it('renders add worked hours button', () => {
    const component = render(<Layout />)
    const addWorkedHoursBtn = component.getByText('Add worked hours')

    expect(addWorkedHoursBtn).toBeInTheDocument()
  })

  it('renders the app name -> WORKEY', () => {
    const component = render(<Layout />)
    const workey = component.getByText('WORKEY')

    expect(workey).toBeInTheDocument()
  })
})
