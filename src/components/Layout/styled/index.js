import styled from 'styled-components'

export const LayoutContainer = styled.div`
  height: 70px;
  padding: 0 15px;
  background-color: #f8f8f8;
  position: relative;
`

export const Nav = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 100%;
  max-width: 800px;
  margin: 0 auto;
`

export const AppName = styled.h2`
  font-size: 22px;
  line-height: 27px;
  font-weight: 800;
  letter-spacing: 1px;
  display: flex;
  justify-content: center;
  align-items: center;

  img {
    width: 25px;
    height: 25px;
    margin-right: 5px;
  }
`

export const ChildrenContainer = styled.div`
  max-width: 800px;
  padding-top: 15px;
  margin: 0 auto;
  padding-bottom: 70px;
`

export const Footer = styled.footer`
  padding: 15px;
  text-align: center;
  font-size: 14px;
  position: fixed;
  background-color: #f8f8f8;
  bottom: 0;
  right: 0;
  left: 0;
`
