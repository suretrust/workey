import React from 'react'
import AUTH from '../../helpers/auth'
import Button from '../Button'
import {
  AppName,
  ChildrenContainer,
  Footer,
  LayoutContainer,
  Nav
} from './styled'

const Layout = ({ children, handleAddWorkedHours }) => {
  const handleSignOut = async e => {
    e.preventDefault()

    try {
      await AUTH.signOut()
    } catch ({ message }) {
      console.error(message)
    }
  }

  return (
    <LayoutContainer data-testid='layout'>
      <Nav>
        <AppName>
          <img src='../../logo.svg' alt='logo' />
          WORKEY
        </AppName>
        <div>
          <Button onClick={handleAddWorkedHours} primary marginRight>
            Add worked hours
          </Button>
          <Button onClick={handleSignOut}>Sign Out</Button>
        </div>
      </Nav>
      <ChildrenContainer>{children}</ChildrenContainer>
      <Footer>
        &#169; {new Date().getFullYear()}{' '}
        <a
          href='http://saheedoladele.com'
          target='_blank'
          rel='noopener noreferrer'
        >
          Saheed Oladele
        </a>
      </Footer>
    </LayoutContainer>
  )
}

export default Layout
