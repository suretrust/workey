import { auth, db } from '../services/firebase'

const addWorkedHours = async data => {
  const { email } = auth().currentUser
  if (!email) return

  await db.ref('timeStamps').push({ ...data, email })
}

const getCurrentUserWorkedHours = async () => {
  const { email } = auth().currentUser
  if (!email) return

  const timeStampRef = db.ref().child('timeStamps').orderByChild('arrivalTime')
  const snapshot = await timeStampRef.once('value')
  const values = snapshot.val()
  if (!values) return

  return Object.values(values).filter(a => a.email === email)
}

const api = { addWorkedHours, getCurrentUserWorkedHours }

export default api
