import { auth } from '../services/firebase'

const signUp = (email, password) => {
  return auth().createUserWithEmailAndPassword(email, password)
}

const signIn = (email, password) => {
  return auth().signInWithEmailAndPassword(email, password)
}

const signOut = () => {
  localStorage.removeItem('workeyUser')
  return auth().signOut()
}

const AUTH = { signUp, signIn, signOut }

export default AUTH
