import {
  getDateFromTimeStamp,
  getEndOfTheDay,
  getHoursFromMilliseconds,
  getLaunchTime,
  getTotalTimeSpentAtWork,
  getWorkedMilliseconds
} from './dateTime'

it('returns launch time', () => {
  expect(getLaunchTime(1, 2)).toBe(1)
})

it('returns total time spent at work', () => {
  expect(getTotalTimeSpentAtWork(1, 2)).toBe(1)
})

it('returns worked hours in milliseconds', () => {
  expect(getWorkedMilliseconds(1, 2)).toBe(-1)
})

it('returns worked hours from milliseconds', () => {
  const milliseconds = 1615842542701

  expect(getHoursFromMilliseconds(milliseconds)).toBe('21.15')
})

it('returns end of the day —> 23:59:59', () => {
  const milliseconds = 1615842542701

  expect(getEndOfTheDay(milliseconds)).toEqual(1615849199999)
})

describe('getDateFromTimeStamp function', () => {
  // Shared functions and values for mocking the global Date so toLocaleString
  // can be mocked. This is so tests always produce the same output no matter
  // what the local time zone is set to in the environment.
  let mockToLocaleString
  const OriginalDate = global.Date
  const mockGlobalDate = (...rest) => {
    const date = new OriginalDate(...rest)
    date.toLocaleStringOriginal = date.toLocaleString
    date.toLocaleString = mockToLocaleString

    return date
  }

  it('returns date in the format "Mar 15, 2021"', () => {
    // 2021-03-15 UTC
    const dateInSeconds = 1615842542701

    // Mock toLocalString to use UTC time zone.
    mockToLocaleString = function (locales, options) {
      options.timeZone = 'UTC'
      return this.toLocaleStringOriginal(locales, options)
    }
    const spyDate = jest
      .spyOn(global, 'Date')
      .mockImplementationOnce(mockGlobalDate)

    // Now that the timeZone is consistent, run the tests.
    const formattedDate = getDateFromTimeStamp(dateInSeconds)

    expect(formattedDate).toEqual('Mar 15, 2021')

    // Restore the original Date implementation.
    spyDate.mockRestore()
  })
})
