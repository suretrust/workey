/**
 *
 * @param {number} startTime in milliseconds
 * @param {number} endTime in milliseonds
 * @returns {number} in milliseconds
 */
export const getLaunchTime = (startTime, endTime) => {
  return endTime - startTime
}

/**
 *
 * @param {number} arrivalTime in milliseconds
 * @param {number} exitTime in milliseconds
 * @returns {number} in milliseconds
 */
export const getTotalTimeSpentAtWork = (arrivalTime, exitTime) => {
  return exitTime - arrivalTime
}

/**
 *
 * @param {number} totalTimeSpentAtWork in milliseconds
 * @param {number} launchTime in milliseconds
 * @returns {number} in milliseconds
 */
export const getWorkedMilliseconds = (totalTimeSpentAtWork, launchTime) => {
  return totalTimeSpentAtWork - launchTime
}

/**
 *
 * @param {number} milliseconds
 * @returns {string} in hours fixed to 2 decimal places
 */
export const getHoursFromMilliseconds = milliseconds => {
  const hours = (milliseconds / (1000 * 60 * 60)) % 24

  return hours.toFixed(2)
}

/**
 *
 * @param {object} durations contains an object
 * of arrivalTime, exitTime, launchStartTime, launchStopTime
 * @returns {number} workedHours fixed to 2 decimal places
 */
export const getWorkedHours = durations => {
  const { arrivalTime, exitTime, launchStartTime, launchStopTime } = durations
  const totalTimeSpentAtWork = getTotalTimeSpentAtWork(arrivalTime, exitTime)
  if (!totalTimeSpentAtWork) return 0

  const totalLaunchTime = getLaunchTime(launchStartTime, launchStopTime)
  if (!totalLaunchTime) return 0

  const workedMilliseconds = getWorkedMilliseconds(
    totalTimeSpentAtWork,
    totalLaunchTime
  )
  if (!workedMilliseconds) return 0

  const workedHours = getHoursFromMilliseconds(workedMilliseconds)
  if (!workedHours) return 0

  return workedHours
}

/**
 *
 * @param {number} date in milliseconds
 * @returns {string} date in the format 'Mar 15, 2021'
 */
export const getDateFromTimeStamp = date => {
  return new Date(date).toLocaleString('en-us', {
    month: 'short',
    day: '2-digit',
    year: 'numeric'
  })
}

/**
 *
 * @param {number} milliseconds
 * @returns {number} —> 23:59:59pm of same date in milliseconds
 */
export const getEndOfTheDay = milliseconds => {
  return new Date(milliseconds).setHours(23, 59, 59, 999)
}

export const getTimeFromMilliseconds = milliseconds => {
  return new Date(milliseconds).toLocaleTimeString('en-US', {
    hour: 'numeric',
    minute: '2-digit'
  })
}
