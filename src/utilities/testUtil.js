import React from 'react'
import { Route, BrowserRouter as Router } from 'react-router-dom'

const TestRoute = ({ children }) => {
  return (
    <Router>
      <Route>{children}</Route>
    </Router>
  )
}

export default TestRoute
