import React, { useState, useEffect } from 'react'
import Skeleton from 'react-loading-skeleton'
import { BrowserRouter as Router, Switch } from 'react-router-dom'
import DailyWorkHours from './pages/DailyWorkHours'
import SignUp from './pages/SignUp'
import Login from './pages/Login'
import { auth } from './services/firebase'
import { PrivateRoute, PublicRoute } from './routes'

// TODO: Add prop type validations
const App = () => {
  const [isLoading, setIsLoading] = useState(true)
  const [isAuthenticated, setIsAuthenticated] = useState(false)

  useEffect(() => {
    auth().onAuthStateChanged(user => {
      setIsAuthenticated(user ? true : false)
      setIsLoading(false)
    })
  }, [])

  return (
    <>
      {isLoading ? (
        <Skeleton count={15} height={70} />
      ) : (
        <Router>
          <Switch>
            <PublicRoute
              exact
              path='/sign-up'
              authenticated={isAuthenticated}
              component={SignUp}
            />
            <PublicRoute
              exact
              path='/login'
              authenticated={isAuthenticated}
              component={Login}
            />
            <PrivateRoute
              path='/'
              authenticated={isAuthenticated}
              component={DailyWorkHours}
            />
          </Switch>
        </Router>
      )}
    </>
  )
}

export default App
