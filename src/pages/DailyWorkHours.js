import React, { useEffect, useState } from 'react'
import Skeleton from 'react-loading-skeleton'
import AddWorkedHoursModal from '../components/AddWorkedHoursModal'
import Layout from '../components/Layout'
import SingleDailyWorkHours from '../components/SingleDailyWorkHours'
import api from '../helpers/api'
import {
  getDateFromTimeStamp,
  getHoursFromMilliseconds,
  getLaunchTime
} from '../utilities/dateTime'

const DailyWorkHours = () => {
  const [workedHours, setWorkedHours] = useState(0)
  const [isLoading, setIsLoading] = useState(true)
  const [allWorkedHours, setAllWorkedHours] = useState([])
  const [daysWithExistingHours, setDaysWithExistingHours] = useState([])
  const [showAddWorkHoursModal, setShowAddWorkHoursModal] = useState(false)

  useEffect(() => {
    getAllWorkedHours()
  }, [workedHours])

  const getAllWorkedHours = async () => {
    try {
      const userWorkedHours = await api.getCurrentUserWorkedHours()
      setDaysWithExistingHours(
        [...userWorkedHours].map(hour => hour.arrivalTime)
      )
      setAllWorkedHours(
        [...userWorkedHours]
          .sort((a, b) => a.arrivalTime - b.arrivalTime)
          .reverse()
      )
      setIsLoading(false)
    } catch ({ message }) {
      console.error(message)
    }
  }

  const handleAddWorkedHours = e => {
    e.preventDefault()

    setShowAddWorkHoursModal(true)
  }

  return (
    <Layout handleAddWorkedHours={handleAddWorkedHours}>
      {showAddWorkHoursModal && (
        <AddWorkedHoursModal
          daysWithExistingHours={daysWithExistingHours}
          setWorkedHours={setWorkedHours}
          setShowAddWorkHoursModal={setShowAddWorkHoursModal}
        />
      )}
      {isLoading ? (
        <Skeleton count={4} height={150} />
      ) : allWorkedHours && allWorkedHours.length ? (
        allWorkedHours.map(timeStamps => {
          const {
            workedHours,
            arrivalTime,
            exitTime,
            launchStartTime,
            launchStopTime
          } = timeStamps
          const launchHours = getHoursFromMilliseconds(
            getLaunchTime(launchStartTime, launchStopTime)
          )
          const dateOfWorkedHour = getDateFromTimeStamp(arrivalTime)

          return (
            <SingleDailyWorkHours
              key={`${workedHours}-${arrivalTime}`}
              workedHours={workedHours}
              arrivalTime={arrivalTime}
              dateOfWorkedHour={dateOfWorkedHour}
              exitTime={exitTime}
              launchHours={launchHours}
            />
          )
        })
      ) : (
        <p>You have not added any worked hours.</p>
      )}
    </Layout>
  )
}

export default DailyWorkHours
