import React, { useState } from 'react'
import LoginSignUpForm from '../components/LoginSignUpForm'
import AUTH from '../helpers/auth'

const SignUp = () => {
  const [userDetails, setUserDetails] = useState({ email: '', password: '' })
  const [error, setError] = useState('')
  const isAnExistingUser =
    'The email address is already in use by another account.'

  const setErrorMessage = message => {
    if (message === isAnExistingUser) {
      return setError('This account already exists, please login.')
    }

    setError(message)
  }

  const handleSubmit = async event => {
    event.preventDefault()
    const { email, password } = userDetails

    try {
      await AUTH.signUp(email, password)
    } catch ({ message }) {
      setErrorMessage(message)
    }
  }

  return (
    <LoginSignUpForm
      setUserDetails={setUserDetails}
      userDetails={userDetails}
      handleSubmit={handleSubmit}
      error={error}
      setError={setError}
      title='Sign Up'
      subtitle='Fill in the form below to create an account.'
      footerText='Already have an account?'
      footerLink={{ text: 'Login', href: '/login' }}
    />
  )
}

export default SignUp
