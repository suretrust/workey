import React, { useState } from 'react'
import LoginSignUpForm from '../components/LoginSignUpForm'
import AUTH from '../helpers/auth'

const Login = () => {
  const [userDetails, setUserDetails] = useState({ email: '', password: '' })
  const [error, setError] = useState('')
  const isNotAnExistingUser =
    'There is no user record corresponding to this identifier. The user may have been deleted.'

  const setErrorMessage = message => {
    if (message === isNotAnExistingUser) {
      return setError('User does not exist, please sign up.')
    }

    setError(message)
  }

  const handleSubmit = async event => {
    event.preventDefault()
    const { email, password } = userDetails

    try {
      await AUTH.signIn(email, password)
    } catch ({ message }) {
      setErrorMessage(message)
    }
  }

  return (
    <LoginSignUpForm
      setUserDetails={setUserDetails}
      userDetails={userDetails}
      handleSubmit={handleSubmit}
      error={error}
      setError={setError}
      title='Login'
      subtitle='Fill in the form below to log in to your account.'
      footerText="Don't have an account?"
      footerLink={{ text: 'Sign Up', href: '/sign-up' }}
    />
  )
}

export default Login
